//
//  LocationMapper.m
//  FoursquareAPI
//
//  Created by Vovan on 30.08.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "LocationMapper.h"
#import "Location.h"

@implementation LocationMapper


+ (Location*)modelFromJson:(NSDictionary*)json{
    Location *res = [Location new];
    
    res.address = json[@"address"];
    res.crossStreet = json[@"crossStreet"];
    res.lat = json[@"lat"];
    res.lng = json[@"lng"];
    res.city = json[@"city"];
    res.state = json[@"state"];
    res.country = json[@"country"];
    //res.formattedAddress = json[@"address"];
    
    return res;
}

@end
