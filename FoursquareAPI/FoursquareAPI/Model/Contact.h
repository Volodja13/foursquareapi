//
//  Contact.h
//  FoursquareAPI
//
//  Created by Vovan on 29.08.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Contact : NSObject

@property(strong, nonatomic) NSString *phone;
@property(strong, nonatomic) NSString *twitter;
@property(strong, nonatomic) NSString *instagram;
@property(strong, nonatomic) NSString *facebook;

@end
