//
//  FotoTableViewCell.m
//  FoursquareAPI
//
//  Created by Vovan on 02.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "FotoTableViewCell.h"
#import "FotoCollectionView.h"

@implementation FotoTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setEmptySetting{
    [self.fotoCollectionView setAlpha:0];
    [self.fotoLabel setAlpha:1];
}

- (void) setNormalSetting{
    [self.fotoCollectionView setAlpha:1];
    [self.fotoLabel setAlpha:0];
}

@end
