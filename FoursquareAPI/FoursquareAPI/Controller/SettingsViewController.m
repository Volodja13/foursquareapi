//
//  SettingsViewController.m
//  FoursquareAPI
//
//  Created by Vovan on 29.08.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "SettingsViewController.h"
#import "MapViewController.h"
#import "Setup.h"
#import "VenueModel.h"

@interface SettingsViewController ()

@property(strong, nonatomic) NSMutableArray *arrayIntent;

@property(weak, nonatomic) MapViewController *mapViewController;

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    UITabBarController *vc = (UITabBarController*)[self parentViewController];
    
    self.mapViewController = vc.viewControllers[0];
    
    
    self.arrayIntent = [@[@"Checkin", @"Global"] mutableCopy];
    
    self.instentPicker.delegate = self;
    self.instentPicker.dataSource = self;
    [self.limitSlider setValue:self.mapViewController.model.setup.limit animated:YES];
    [self.radiusSlider setValue:self.mapViewController.model.setup.radius animated:YES];
    [self.instentPicker selectRow:self.mapViewController.model.setup.intent inComponent:0 animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return  1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [self.arrayIntent count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return self.arrayIntent[row];
}


- (IBAction)radiusAction:(id)sender {
    self.mapViewController.model.setup.radius = (long) self.radiusSlider.value;
}


- (IBAction)limitAction:(id)sender {
    self.mapViewController.model.setup.limit = (long) self.limitSlider.value;
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    self.mapViewController.model.setup.intent = row;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
