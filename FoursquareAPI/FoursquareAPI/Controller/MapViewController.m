//
//  MapViewController.m
//  FoursquareAPI
//
//  Created by Vovan on 29.08.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "MapViewController.h"
#import "VenuePointAnnotation.h"
#import "ListViewController.h"
#import "VenueModel.h"
#import "DetailViewController.h"
#import "Location.h"

@interface MapViewController ()


@property (strong, nonatomic) CLLocationManager *locationManager;

@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self->_model = [[VenueModel alloc] init];
    
    [self setupLocation];
    
    //self.mapKit.showsCompass = YES;
    self.mapKit.delegate = self;
    self.mapKit.showsUserLocation = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setupLocation{
    if([CLLocationManager locationServicesEnabled]){
        self->_locationManager = [[CLLocationManager alloc] init];
        self->_locationManager.delegate = self;
        
        CLAuthorizationStatus authorizationStatus = [CLLocationManager authorizationStatus];
        if(authorizationStatus == kCLAuthorizationStatusNotDetermined)
        {
            [self.locationManager requestWhenInUseAuthorization];
        }
        else
        {
            [self runUpdatingLocation];
        }
    }
    else{
        
    }
}

- (void)runUpdatingLocation
{
    [self.locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if((status == kCLAuthorizationStatusAuthorizedAlways) || (status == kCLAuthorizationStatusAuthorizedWhenInUse))
    {
        [self runUpdatingLocation];
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    if(self.mapKit.showsUserLocation == NO){
        [self.mapKit setShowsUserLocation:YES];
        
        CLLocation *initLocation = [locations lastObject];
        [self.mapKit setRegion:MKCoordinateRegionMakeWithDistance([initLocation coordinate], 2500, 2500) animated:YES];
        
        [self.model loadVenueListWithLocation:initLocation CompletitionBlock:^void (NSMutableArray *venues){
            [self addVenuesAnnotation:venues];
            
            [((ListViewController*)((UITabBarController*)self.parentViewController).viewControllers[1]).tableView reloadData];
        }];
        
        [self.locationManager stopUpdatingLocation];
        [self.mapKit setShowsCompass:YES];
        [self.mapKit setShowsScale:YES];
    }
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation{
    if(annotation == mapView.userLocation){
        return nil;
    }
    static NSString *annotaionIdentifier=@"annotationIdentifier";
    MKPinAnnotationView *aView=(MKPinAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:annotaionIdentifier ];
    if (aView==nil) {
        
        aView=[[MKPinAnnotationView alloc]initWithAnnotation:annotation reuseIdentifier:annotaionIdentifier];
        aView.pinColor = MKPinAnnotationColorGreen;
        aView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        //        aView.image=[UIImage imageNamed:@"arrow"]; you can give here  your own image
        aView.animatesDrop=TRUE;
        aView.canShowCallout = YES;
        aView.calloutOffset = CGPointMake(-5, 5);
    }
    return aView;
    //return nil;
}

- (void) addVenuesAnnotation:(NSMutableArray*)venues{
    for(Venue *venue in venues){
        VenuePointAnnotation *annotaton = [VenuePointAnnotation new];
        annotaton.coordinate = CLLocationCoordinate2DMake([venue.location.lat doubleValue], [venue.location.lng doubleValue]);
        annotaton.title = venue.name;
        annotaton.subtitle = venue.location.address;
        
        annotaton.venue = venue;
        
        [self.mapKit addAnnotation:annotaton];
    }
}

-(void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    DetailViewController *detailController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"DVC"];
    detailController.venue = ((VenuePointAnnotation*)[mapView.selectedAnnotations lastObject]).venue;
    detailController.mapViewController = self;
    detailController.block = ^{};
    [self presentViewController:detailController animated:YES completion:^{}];
}


- (void) reloadData{
    [self.mapKit removeAnnotations:self.mapKit.annotations];
    [self.mapKit setShowsUserLocation:NO];
    [self runUpdatingLocation];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
