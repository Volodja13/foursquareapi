//
//  VenueMapper.m
//  FoursquareAPI
//
//  Created by Vovan on 30.08.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "VenueMapper.h"
#import "LocationMapper.h"
#import "ContactMapper.h"
#import "Venue.h"


@implementation VenueMapper


+ (NSMutableArray*)modelFromJson:(NSDictionary*)json{
    NSMutableArray *res = [@[] mutableCopy];
    
    NSDictionary *response = json[@"response"];
    
    for(NSDictionary *venue in response[@"venues"]){
        Venue *ven = [Venue new];
        ven.VenueId = venue[@"id"];
        ven.name = venue[@"name"];
        NSArray *categorys = venue[@"categories"];
        NSDictionary *category = [categorys firstObject];
        ven.categoryName = category[@"pluralName"];
        ven.contact = [ContactMapper modelFromJson:venue[@"contact"]];
        ven.location = [LocationMapper modelFromJson:venue[@"location"]];
        [res addObject:ven];
    }
    
    return res;
}

@end
