//
//  Tip.h
//  FoursquareAPI
//
//  Created by Vovan on 01.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Tip : NSObject

@property(strong, nonatomic) NSString *text;
@property(strong, nonatomic) NSString *author;

@end
