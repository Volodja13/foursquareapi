//
//  FotoCollectionViewCell.m
//  FoursquareAPI
//
//  Created by Vovan on 02.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "FotoCollectionViewCell.h"
#import "Foto.h"

@interface FotoCollectionViewCell ()

@property(strong, nonatomic) Foto *downFoto;

@end

@implementation FotoCollectionViewCell


- (void) setFoto:(Foto*)foto{
    if(_downFoto.imageUrl == foto.imageUrl){
        self.imageView.image = foto.image;
    }
    _downFoto = foto;
}



@end
