//
//  FotoCollectionView.h
//  FoursquareAPI
//
//  Created by Vovan on 02.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewController;

@interface FotoCollectionView : UICollectionView <UICollectionViewDelegate, UICollectionViewDataSource>

@property(weak, nonatomic) DetailViewController *detailViewController;

@end
