//
//  FotoCollectionView.m
//  FoursquareAPI
//
//  Created by Vovan on 02.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "FotoCollectionView.h"
#import "DetailViewController.h"
#import "FotoCollectionViewCell.h"
#import "Foto.h"
#import "MapViewController.h"
#import "FotoViewController.h"
#import "VenueModel.h"

@implementation FotoCollectionView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [self.detailViewController.arrayFotosUrl count];
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FotoCollectionViewCellId" forIndexPath:indexPath];
    
    ((FotoCollectionViewCell*)cell).imageView.image = nil;
    
    Foto *f = self.detailViewController.arrayFotosUrl[indexPath.row];
    
    NSURL *url = f.imageUrl;
    
    
    [((FotoCollectionViewCell*)cell) setFoto:f];
    
    [self.detailViewController.operationQueue addOperationWithBlock:^{
        
        [self.detailViewController.mapViewController.model loadFotoForURL:url CompletitionBlock:^(Foto *foto) {
            [((FotoCollectionViewCell*)cell) setFoto:foto];
        }];
    }];
    
    
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    FotoCollectionViewCell* cell = (FotoCollectionViewCell*)[collectionView cellForItemAtIndexPath:indexPath];
    
    if(cell.imageView.image != nil){
        
        FotoViewController *fotoController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"FVC"];
        [fotoController setImage: cell.imageView.image];
        [self.detailViewController presentViewController:fotoController animated:YES completion:^{}];
        
    }
}

@end
