//
//  NameVenueTableViewCell.m
//  FoursquareAPI
//
//  Created by Vovan on 01.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "NameVenueTableViewCell.h"
#import "Venue.h"
#import "Location.h"

@interface NameVenueTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *addresLabel;
@property (weak, nonatomic) IBOutlet UILabel *likesLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activitiIndicator;

@end

@implementation NameVenueTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



- (void) setVenue:(Venue*)venue{
    self.nameLabel.text = [venue.name isEqualToString:@""] ? @"" :venue.name;
    self.categoryLabel.text = [venue.categoryName  isEqualToString:@""] ? @"" :venue.categoryName;
    self.addresLabel.text = [NSString stringWithFormat:@"%@ %@ %@ %@", (venue.location.country == nil) ? @" " :venue.location.country, (venue.location.state == nil) ? @" " :venue.location.state, (venue.location.city == nil) ? @" " :venue.location.city, (venue.location.address == nil) ? @" " :venue.location.address];
    self.likesLabel.text = @" ";
    [self.activitiIndicator startAnimating];
}


- (void) setCountLikes:(NSNumber*)count{
    [self.activitiIndicator stopAnimating];
    self.likesLabel.text = [NSString stringWithFormat:@"Likes: %@", count];
}

@end
