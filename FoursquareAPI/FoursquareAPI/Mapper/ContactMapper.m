//
//  ContactMapper.m
//  FoursquareAPI
//
//  Created by Vovan on 30.08.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "ContactMapper.h"
#import "Contact.h"

@implementation ContactMapper


+ (Contact*)modelFromJson:(NSDictionary*)json{
    Contact *res = [Contact new];
    if(nil == json[@"formattedPhone"]){
        res.phone = json[@"formattedPhone"];
    }
    if(nil == json[@"twitter"]){
        res.twitter = json[@"twitter"];
    }
    if(nil == json[@"facebookName"]){
        res.facebook = json[@"facebookName"];
    }
    if(nil == json[@"instagram"]){
        res.instagram = json[@"instagram"];
    }
    return res;
}

@end
