//
//  MapViewController.h
//  FoursquareAPI
//
//  Created by Vovan on 29.08.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@class VenueModel;

@interface MapViewController : UIViewController <CLLocationManagerDelegate, MKMapViewDelegate>

@property (strong, nonatomic) VenueModel *model;
@property (weak, nonatomic) IBOutlet MKMapView *mapKit;

- (void) reloadData;

@end
