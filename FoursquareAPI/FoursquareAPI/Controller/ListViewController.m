//
//  ListViewController.m
//  FoursquareAPI
//
//  Created by Vovan on 03.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "ListViewController.h"
#import "MapViewController.h"
#import "VenueListTableViewCell.h"
#import "VenueModel.h"
#import "DetailViewController.h"

@interface ListViewController ()

@property(weak, nonatomic) MapViewController *mapViewController;

@end

@implementation ListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITabBarController *vc = (UITabBarController*)[self parentViewController];
    
    self.mapViewController = vc.viewControllers[0];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.mapViewController.model.venues count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"VenueListTableViewCellId" forIndexPath:indexPath];
    Venue *ven = self.mapViewController.model.venues[indexPath.row];
    //  ((VenueListTableViewCell*)cell).venue = ven;
    [((VenueListTableViewCell*)cell) setVenue:ven];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 94;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Venue *ven = self.mapViewController.model.venues[indexPath.row];
    DetailViewController *detailController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"DVC"];
    detailController.venue = ven;
    detailController.block = ^{
        
    };
    detailController.mapViewController = self.mapViewController;
    [self presentViewController:detailController animated:YES completion:^{}];
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
