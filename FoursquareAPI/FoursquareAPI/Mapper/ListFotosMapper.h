//
//  ListFotosMapper.h
//  FoursquareAPI
//
//  Created by Vovan on 02.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ListFotosMapper : NSObject


+ (NSMutableArray*)modelFromJson:(NSDictionary*)json;

@end
