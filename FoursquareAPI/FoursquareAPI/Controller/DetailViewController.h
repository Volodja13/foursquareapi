//
//  DetailViewController.h
//  FoursquareAPI
//
//  Created by Vovan on 30.08.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MapViewController;
@class Venue;

typedef void(^FinishBlock)();

@interface DetailViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) Venue *venue;
@property (weak, nonatomic) MapViewController *mapViewController;
@property(strong, nonatomic) NSMutableArray *arrayFotosUrl;
@property(strong, nonatomic) NSOperationQueue *operationQueue;
@property(strong, nonatomic) FinishBlock block;

@end
