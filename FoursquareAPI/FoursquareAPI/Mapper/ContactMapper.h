//
//  ContactMapper.h
//  FoursquareAPI
//
//  Created by Vovan on 30.08.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Contact;

@interface ContactMapper : NSObject

+ (Contact*)modelFromJson:(NSDictionary*)json;

@end
