//
//  VenueModel.m
//  FoursquareAPI
//
//  Created by Vovan on 29.08.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "VenueModel.h"
#import "LikesMapper.h"
#import "TipsMapper.h"
#import "ListFotosMapper.h"
#import <UIKit/UIKit.h>
#import "Foto.h"
#import "VenueMO+CoreDataClass.h"
#import "AppDelegate.h"
#import "SetupMO+CoreDataClass.h"
#import "Setup.h"
#import "Venue.h"
#import "VenueMapper.h"
#import "Location.h"
#import "Contact.h"

@interface VenueModel ()

@property(nonatomic, strong) NSString *baseUrl;
@property(nonatomic, strong) NSString *client_id;
@property(nonatomic, strong) NSString *client_secret;

@end

@implementation VenueModel


- (instancetype) init{
    self = [super init];
    if(self)
    {
        self->_baseUrl = @"https://api.foursquare.com/v2/venues/";
        self->_client_id = @"FV1PQ0WOTV5RYQTB2BDAV1HVQFIIHAOR1FKOT4HGVCKQ1PER";
        self->_client_secret = @"P1ODBSDTVNVT44DC1GKDSAGQYIT0WPMECXQRUIUFUFK2H2RX";
        self->_venues = [@[] mutableCopy];
        self->_saveVenues = [@[] mutableCopy];
        self->_saveVenues = [self loadSavedVenue];
        self->_setup = [Setup new];
        self->_setup = [self loadSetup];
    }
    return self;
}

- (void) showAlertInternetConnection{
    UIAlertView *theAlert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                       message:@"No internet connection."
                                                      delegate:self
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil];
    [theAlert show];
}

- (void) loadVenueListWithLocation:(CLLocation*) location CompletitionBlock:(LoadVenueListCompletitionBlock) block{
    
    NSString *intentString = @"checkin";
    if(self.setup.intent == 1)
    {
        intentString = @"global";
    }
    
    NSString *urlString = [NSString stringWithFormat:@"%@search?v=20161016&ll=%f%%2C%%20%f&limit=%ld&radius=%ld&intent=%@&client_id=%@&client_secret=%@", self.baseUrl, location.coordinate.latitude, location.coordinate.longitude, (long)self.setup.limit, (long)self.setup.radius, intentString, self.client_id,self.client_secret];
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    
        [NSURLConnection sendAsynchronousRequest:request queue: [NSOperationQueue currentQueue] completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
            
            NSError *error;
            if(response == nil){
                self.venues = [@[] mutableCopy];
                [self showAlertInternetConnection];
                return ;
            }
            NSMutableDictionary *dJSON = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            self.venues = [VenueMapper modelFromJson:dJSON];
            
            for(Venue* venue in self.venues){
                for (Venue *saveVenue in self.saveVenues) {
                    if([venue.VenueId isEqualToString:saveVenue.VenueId]){
                        venue.use = YES;
                        break;
                    }
                }
            }
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                block(self.venues);
            });
        }];
    });
}

- (void) loadCountLikesForId:(NSString*)venueId CompletitionBlock:(LoadVenueCountLikesCompletitionBlock)block{
    NSString *urlString = [NSString stringWithFormat:@"%@%@/likes?v=20161016&client_id=%@&client_secret=%@", self.baseUrl, venueId, self.client_id,self.client_secret];
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [NSURLConnection sendAsynchronousRequest:request queue: [NSOperationQueue currentQueue] completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
            if(response == nil){
                //self.venues = [@[] mutableCopy];
              //  [self showAlertInternetConnection];
                return ;
            }
            NSError *error;
            NSMutableDictionary *dJSON = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            NSNumber *num = [LikesMapper modelFromJson:dJSON];
            dispatch_async(dispatch_get_main_queue(), ^{
                block(num);
            });
        }];
    });
}


- (void) loadTipsForId:(NSString*)venueId CompletitionBlock:(LoadVenueTipsCompletitionBlock)block{
    NSString *urlString = [NSString stringWithFormat:@"%@%@/tips?sort=popular&limit=50&v=20161016&client_id=%@&client_secret=%@", self.baseUrl, venueId, self.client_id,self.client_secret];
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [NSURLConnection sendAsynchronousRequest:request queue: [NSOperationQueue currentQueue] completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
            NSMutableArray *ret = [@[] mutableCopy];
            if(response == nil){
                //self.venues = [@[] mutableCopy];
              //  [self showAlertInternetConnection];
                dispatch_async(dispatch_get_main_queue(), ^{
                    block(ret);
                });
                //return ;
            }
            else{
                NSError *error;
                NSMutableDictionary *dJSON = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                // NSNumber *num = [LikesMapper modelFromJson:dJSON];
                ret = [TipsMapper modelFromJson:dJSON];
                dispatch_async(dispatch_get_main_queue(), ^{
                    block(ret);
                });
            }
        }];
    });
}


- (void) loadListFotosForId:(NSString*)venueId CompletitionBlock:(LoadVenueListFotosCompletitionBlock)block{
    NSString *urlString = [NSString stringWithFormat:@"%@%@/photos?limit=50&v=20161016&client_id=%@&client_secret=%@", self.baseUrl, venueId, self.client_id,self.client_secret];
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [NSURLConnection sendAsynchronousRequest:request queue: [NSOperationQueue currentQueue] completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
            NSMutableArray *ret = [@[] mutableCopy];
            if(response == nil){
                //self.venues = [@[] mutableCopy];
               // [self showAlertInternetConnection];
                dispatch_async(dispatch_get_main_queue(), ^{
                    block(ret);
                });
                //return ;
            }
            else{
                NSError *error;
                NSMutableDictionary *dJSON = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            // NSNumber *num = [LikesMapper modelFromJson:dJSON];
                ret = [ListFotosMapper modelFromJson:dJSON];//[TipsMapper modelFromJson:dJSON];
                dispatch_async(dispatch_get_main_queue(), ^{
                    block(ret);
                });
            }
        }];
    });
}

- (void) loadFotoForURL:(NSURL*)url CompletitionBlock:(LoadFotoCompletitionBlock)block{
    //NSURLRequest *request = [NSURLRequest requestWithURL:url];
    dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:url];
        request.cachePolicy = NSURLRequestReturnCacheDataElseLoad;
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          if (data == nil) {
                                              //[self printCannotLoad];
                                          } else {
                                              UIImage *image = [UIImage imageWithData:data];
                                              Foto *foto = [Foto new];
                                              foto.image = image;
                                              foto.imageUrl = url;
                                                          
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  block(foto);
                                              });
                                          }
                                      }];
        [task resume];
        
//        
//        [NSURLConnection sendAsynchronousRequest:request queue: [NSOperationQueue currentQueue] completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
//            
//            UIImage *image = [UIImage imageWithData:data];
//            Foto *foto = [Foto new];
//            foto.image = image;
//            foto.imageUrl = url;
//            
//            dispatch_async(dispatch_get_main_queue(), ^{
//                block(foto);
//            });
//        }];
    });
}


- (NSMutableArray*) loadSavedVenue{
    NSMutableArray *res = [@[] mutableCopy];
    NSFetchRequest <VenueMO*> *fetchRequest = [VenueMO fetchRequest];
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSArray *venuesMO = [NSArray arrayWithArray:[appDelegate.persistentContainer.viewContext
                                              executeFetchRequest:fetchRequest
                                              error:nil]];
    for (VenueMO *venueMO in venuesMO) {
        Venue *venue = [Venue new];
        venue.VenueId = venueMO.venueId;
        venue.name = venueMO.name;
        venue.categoryName = venueMO.categoryName;
        venue.use = YES;
        Contact *contact = [Contact new];
        contact.phone = venueMO.phone;
        contact.twitter = venueMO.twiter;
        contact.instagram = venueMO.instagram;
        contact.facebook = venueMO.facebook;
        venue.contact = contact;
        Location *location = [Location new];
        location.address = venueMO.address;
        location.crossStreet = venueMO.crossStreet;
        location.lat = [NSNumber numberWithDouble: venueMO.lat];
        location.lng = [NSNumber numberWithDouble: venueMO.lng];
        location.city = venueMO.city;
        location.state = venueMO.state;
        location.country = venueMO.country;
        location.formattedAddress = venueMO.formattedAddress;
        venue.location = location;
        [res addObject:venue];
    }
    return res;
}

- (void) savedVenue:(Venue*)venue{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
    NSEntityDescription *venueDescr = [NSEntityDescription entityForName:@"Venue" inManagedObjectContext:context];
    
    VenueMO *venueMO = [[VenueMO alloc] initWithEntity:venueDescr insertIntoManagedObjectContext:context];
    
    venueMO.venueId = venue.VenueId;
    venueMO.name = venue.name;
    venueMO.categoryName = venue.categoryName;
    venueMO.phone = venue.contact.phone;
    venueMO.twiter = venue.contact.twitter;
    venueMO.instagram = venue.contact.instagram;
    venueMO.facebook = venue.contact.facebook;
    venueMO.address = venue.location.address;
    venueMO.crossStreet = venue.location.crossStreet;
    venueMO.lat = [venue.location.lat doubleValue];
    venueMO.lng = [venue.location.lng doubleValue];
    venueMO.city = venue.location.city;
    venueMO.state = venue.location.state;
    venueMO.country = venue.location.country;
    venueMO.formattedAddress = venue.location.formattedAddress;
    
    [appDelegate saveContext];
    self.saveVenues = [self loadSavedVenue];
    for (Venue* v in self.venues) {
        if(v.VenueId == venue.VenueId){
            v.use = YES;
            
        }
    }
}

- (void) unsavedVenue:(Venue*)venue{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
    
    NSFetchRequest <VenueMO*> *fetchRequest = [VenueMO fetchRequest];
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"venueId == %@", venue.VenueId]];
    
    NSError *error;
    NSArray *result = [context executeFetchRequest:fetchRequest error:&error];
    [context deleteObject:result[0]];
    [appDelegate saveContext];
    self.saveVenues = [self loadSavedVenue];
    for (Venue* v in self.venues) {
        if(v.VenueId == venue.VenueId){
            v.use = NO;
            
        }
    }
}


- (Setup*) loadSetup{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
    NSFetchRequest <SetupMO*> *fetchRequest = [SetupMO fetchRequest];
    
    NSError *error;
    NSArray *result = [context executeFetchRequest:fetchRequest error:&error];
    Setup *setup = [Setup new];
    if([result count] == 0){
        setup.limit = (long)50;
        setup.radius = (long)5000;
        setup.intent = (long)0;
    }
    else{
        SetupMO *setupMO = result[0];
        setup.limit = setupMO.limit;
        setup.radius = setupMO.radius;
        setup.intent = setupMO.intent;
    }
    return setup;
    
}

- (void) saveSetup:(Setup*)setup{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
    NSEntityDescription *setupDescr = [NSEntityDescription entityForName:@"Setup" inManagedObjectContext:context];
    NSFetchRequest <SetupMO*> *fetchRequest = [SetupMO fetchRequest];
    
    NSError *error;
    NSArray *result = [context executeFetchRequest:fetchRequest error:&error];
    
    if([result count] == 0){
        SetupMO *setupMO = [[SetupMO alloc] initWithEntity:setupDescr insertIntoManagedObjectContext:context];
        
        setupMO.radius = (int)setup.radius;
        setupMO.limit = (int)setup.limit;
        setupMO.intent = (int)setup.intent;
    }
    else{
        SetupMO *setupMO = result[0];
        setupMO.limit = (int)setup.limit;
        setupMO.radius = (int)setup.radius;
        setupMO.intent = (int)setup.intent;

    }
    [appDelegate saveContext];
}

@end
