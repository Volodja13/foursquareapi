//
//  DetailViewController.m
//  FoursquareAPI
//
//  Created by Vovan on 30.08.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "DetailViewController.h"
#import "VenueModel.h"
#import "NameVenueTableViewCell.h"
#import "MapViewController.h"
#import "TipTableViewCell.h"
#import "FotoTableViewCell.h"
#import "FotoCollectionView.h"
#import "Tip.h"
#import "Venue.h"
#import "Location.h"

@interface DetailViewController ()

@property (weak, nonatomic) IBOutlet UINavigationItem *navigationBar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveButton;


@property(strong, nonatomic) NSMutableArray *arrayTips;

@property(assign, nonatomic) NSInteger countLikes;

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _countLikes = -1;
    self.tableView.allowsSelection = NO;
    // Do any additional setup after loading the view.
//    self.venueId.text = self.venue.VenueId;
//    self.name.text = self.venue.name;
    self.navigationBar.title = self.venue.name;
    self.arrayTips = [@[] mutableCopy];
    self.arrayFotosUrl = [@[] mutableCopy];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.saveButton.title = @"Save";
    for (Venue *ven in self.mapViewController.model.saveVenues
) {
        if([ven.VenueId isEqualToString: self.venue.VenueId])
        {
            self.saveButton.title = @"Unsave";
            break;
        }
    }
    
    self.operationQueue = [NSOperationQueue new];
    
    [self.operationQueue addOperationWithBlock:^{
        
        [self.mapViewController.model loadTipsForId:self.venue.VenueId CompletitionBlock:^(NSMutableArray *tips){
            
            self.arrayTips = tips;
            [self.tableView reloadData];
        
        }];
        
    }];
    [self.operationQueue addOperationWithBlock:^{
        
        [self.mapViewController.model loadCountLikesForId:self.venue.VenueId CompletitionBlock:^(NSNumber *count) {
            self.countLikes = [count integerValue];
            [(NameVenueTableViewCell*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]]  setCountLikes:count];
        }];
    }];
    [self.operationQueue addOperationWithBlock:^{
        
        [self.mapViewController.model loadListFotosForId:self.venue.VenueId CompletitionBlock:^(NSMutableArray *fotos) {
            self.arrayFotosUrl = fotos;
            [((FotoTableViewCell*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]]).fotoCollectionView reloadData];
            if([self.arrayFotosUrl count] == 0)
            {
                [((FotoTableViewCell*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]]) setEmptySetting];
            }
            else{
                [((FotoTableViewCell*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]]) setNormalSetting];
            }
        }];
        
    }];
}
- (IBAction)cancelAction:(id)sender {
    [self.operationQueue cancelAllOperations];
    self.block();
    [self dismissViewControllerAnimated:YES completion:^{}];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //return 1;
    NSInteger count = 2 + (([self.arrayTips count] == 0) ? 1 : [self.arrayTips count]);
    return count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    if(indexPath.row == 0)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"NameVenueId" forIndexPath:indexPath];
        [(NameVenueTableViewCell*)cell setVenue:self.venue];
        if(self.countLikes != -1){
            [(NameVenueTableViewCell*)cell  setCountLikes:[NSNumber numberWithInt: (int)self.countLikes]];
        }
    }
    else if(indexPath.row == 1)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"FotoTableViewCellId" forIndexPath:indexPath];
//        ((FotoTableViewCell*)cell).fotoCollectionView = [FotoCollectionView new];
        ((FotoTableViewCell*)cell).fotoCollectionView.delegate = ((FotoTableViewCell*)cell).fotoCollectionView;
        ((FotoTableViewCell*)cell).fotoCollectionView.dataSource = ((FotoTableViewCell*)cell).fotoCollectionView;
        ((FotoTableViewCell*)cell).fotoCollectionView.detailViewController = self;
    }
    else{
        if([self.arrayTips count] == 0)
        {
            Tip *tip = [Tip new];
            tip.author = @"No tips";
            cell = [tableView dequeueReusableCellWithIdentifier:@"TipCellId" forIndexPath:indexPath];
            [(TipTableViewCell*)cell setTip:tip];
        }
        else
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"TipCellId" forIndexPath:indexPath];
            [(TipTableViewCell*)cell setTip:self.arrayTips[indexPath.row - 2]];
        }
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0)
    {
        return 157;
    }
    else if(indexPath.row == 1)
    {
        return 157;
    }
    else{
        if([self.arrayTips count] == 0)
        {
            return 37 + 4;
        }
        else
        {
            return 37 + 8 + [TipTableViewCell heightForText:((Tip*)self.arrayTips[indexPath.row - 2]).text];
        }
        
    }
}

- (IBAction)saveAction:(id)sender {
    if([self.saveButton.title isEqualToString:@"Save"])
    {
        self.saveButton.title = @"Unsave";
        [self.mapViewController.model savedVenue:self.venue];
    }
    else{
        self.saveButton.title = @"Save";
        [self.mapViewController.model unsavedVenue:self.venue];
    }
    
}

- (IBAction)goAction:(id)sender {
    MKPlacemark *mark = [[MKPlacemark alloc] initWithCoordinate:CLLocationCoordinate2DMake([self.venue.location.lat doubleValue], [self.venue.location.lng doubleValue])];
    MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:mark];
    mapItem.name = self.venue.name;
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:MKLaunchOptionsDirectionsModeDriving, MKLaunchOptionsDirectionsModeKey, nil];
    [mapItem openInMapsWithLaunchOptions:dict];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
