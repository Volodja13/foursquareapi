//
//  VenuePointAnnotation.h
//  FoursquareAPI
//
//  Created by Vovan on 31.08.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "Venue.h"

@interface VenuePointAnnotation : MKPointAnnotation

@property (strong, nonatomic) Venue *venue;

@end
