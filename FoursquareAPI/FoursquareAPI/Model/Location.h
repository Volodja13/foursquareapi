//
//  Location.h
//  FoursquareAPI
//
//  Created by Vovan on 29.08.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Location : NSObject

@property(strong, nonatomic) NSString *address;
@property(strong, nonatomic) NSString *crossStreet;
@property(strong, nonatomic) NSNumber *lat;
@property(strong, nonatomic) NSNumber *lng;
@property(strong, nonatomic) NSString *city;
@property(strong, nonatomic) NSString *state;
@property(strong, nonatomic) NSString *country;
@property(strong, nonatomic) NSString *formattedAddress;

@end
