//
//  LikesMapper.m
//  FoursquareAPI
//
//  Created by Vovan on 01.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "LikesMapper.h"

@implementation LikesMapper


+ (NSNumber*)modelFromJson:(NSDictionary*)json{
    NSDictionary *response = json[@"response"];
    NSDictionary *likes = response[@"likes"];
    NSNumber *res = likes[@"count"];
    return res;
}

@end
