//
//  Foto.h
//  FoursquareAPI
//
//  Created by Vovan on 02.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Foto : NSObject

@property(strong, nonatomic) NSURL *imageUrl;
@property(strong, nonatomic) UIImage *image;

@end
