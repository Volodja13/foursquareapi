//
//  VenueListTableViewCell.m
//  FoursquareAPI
//
//  Created by Vovan on 03.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "VenueListTableViewCell.h"
#import "Venue.h"
#import "Location.h"

@interface VenueListTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *adressLabel;
@property (weak, nonatomic) IBOutlet UILabel *catagoryLabel;

@end

@implementation VenueListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void) setVenue:(Venue*)venue{
    self.nameLabel.text = venue.name;
    self.catagoryLabel.text = venue.categoryName;
    self.adressLabel.text = venue.location.address;
}

@end
