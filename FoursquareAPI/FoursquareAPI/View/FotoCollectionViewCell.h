//
//  FotoCollectionViewCell.h
//  FoursquareAPI
//
//  Created by Vovan on 02.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Foto;

@interface FotoCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;


- (void) setFoto:(Foto*)foto;

@end
