//
//  ListFotosMapper.m
//  FoursquareAPI
//
//  Created by Vovan on 02.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "ListFotosMapper.h"
#import "Foto.h"


@implementation ListFotosMapper


+ (NSMutableArray*)modelFromJson:(NSDictionary*)json{
    
    NSMutableArray *res = [@[] mutableCopy];
    
    NSDictionary *response = json[@"response"];
    
    NSDictionary *fotos = response[@"photos"];
    
    
    for(NSDictionary *foto in fotos[@"items"]){
        
        Foto *f = [Foto new];
        f.imageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@x%@%@",foto[@"prefix"],foto[@"width"],foto[@"height"],foto[@"suffix"]]];
        f.image = nil;
        [res addObject:f];
    }
    
    return res;
}

@end
