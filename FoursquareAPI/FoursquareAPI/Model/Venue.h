//
//  Venue.h
//  FoursquareAPI
//
//  Created by Vovan on 29.08.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Contact;
@class Location;

@interface Venue : NSObject

@property(strong, nonatomic) NSString *VenueId;
@property(strong, nonatomic) NSString *name;
@property(strong, nonatomic) NSString *categoryName;
@property(strong, nonatomic) Contact *contact;
@property(strong, nonatomic) Location *location;
@property(assign, nonatomic) BOOL use;

@end
