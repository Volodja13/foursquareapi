//
//  MainTabBarController.m
//  FoursquareAPI
//
//  Created by Vovan on 03.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "MainTabBarController.h"
#import "FavoritesViewController.h"
#import "MapViewController.h"
#import "VenueModel.h"

@interface MainTabBarController ()

@end

@implementation MainTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.delegate = self;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
    [((FavoritesViewController*)self.viewControllers[2]).tableView reloadData];
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
{
    if(tabBarController.selectedIndex == 3)
    {
        MapViewController *mvc = tabBarController.viewControllers[0];
        [mvc.model saveSetup:mvc.model.setup];
        [mvc reloadData];
    }
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
