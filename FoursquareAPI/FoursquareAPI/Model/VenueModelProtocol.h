//
//  VenueModelProtocol.h
//  FoursquareAPI
//
//  Created by Vovan on 29.08.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@class Foto;
@class Venue;
@class Setup;

typedef void (^LoadVenueListCompletitionBlock)(NSMutableArray *venues);
typedef void (^LoadVenueCountLikesCompletitionBlock)(NSNumber *count);
typedef void (^LoadVenueTipsCompletitionBlock)(NSMutableArray *tips);
typedef void (^LoadVenueListFotosCompletitionBlock)(NSMutableArray *fotos);
typedef void (^LoadFotoCompletitionBlock)(Foto *foto);

@protocol VenueModelProtocol <NSObject>

@required

- (void) loadVenueListWithLocation:(CLLocation*) location CompletitionBlock:(LoadVenueListCompletitionBlock) block;

- (void) loadCountLikesForId:(NSString*)venueId CompletitionBlock:(LoadVenueCountLikesCompletitionBlock)block;

- (void) loadTipsForId:(NSString*)venueId CompletitionBlock:(LoadVenueTipsCompletitionBlock)block;


- (void) loadListFotosForId:(NSString*)venueId CompletitionBlock:(LoadVenueListFotosCompletitionBlock)block;

- (void) loadFotoForURL:(NSURL*)url CompletitionBlock:(LoadFotoCompletitionBlock)block;

- (NSMutableArray*) loadSavedVenue;

- (void) savedVenue:(Venue*)venue;

- (void) unsavedVenue:(Venue*)venue;

- (Setup*) loadSetup;

- (void) saveSetup:(Setup*)setup;

@end
