//
//  VenueListTableViewCell.h
//  FoursquareAPI
//
//  Created by Vovan on 03.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Venue;

@interface VenueListTableViewCell : UITableViewCell

- (void) setVenue:(Venue*)venue;

@end
