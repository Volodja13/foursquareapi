//
//  Setup.h
//  FoursquareAPI
//
//  Created by Vovan on 04.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Setup : NSObject

@property(assign, nonatomic) NSInteger radius;
@property(assign, nonatomic) NSInteger limit;
@property(assign, nonatomic) NSInteger intent;

@end
