//
//  VenueMapper.h
//  FoursquareAPI
//
//  Created by Vovan on 30.08.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface VenueMapper : NSObject

+ (NSMutableArray*)modelFromJson:(NSDictionary*)json;

@end
