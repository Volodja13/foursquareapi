//
//  TipsMapper.m
//  FoursquareAPI
//
//  Created by Vovan on 01.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "TipsMapper.h"
#import "Tip.h"

@implementation TipsMapper


+ (NSMutableArray*)modelFromJson:(NSDictionary*)json{
    
    NSMutableArray *res = [@[] mutableCopy];
    
    NSDictionary *response = json[@"response"];
    
    NSDictionary *tips = response[@"tips"];
    
    
    for(NSDictionary *tip in tips[@"items"]){
        Tip *t = [Tip new];
        t.text = tip[@"text"];
        NSDictionary *user = tip[@"user"];
        NSString *firstName = user[@"firstName"];
        NSString *lastName = user[@"firstName"];
        
        t.author = [NSString stringWithFormat:@"%@ %@",(firstName == nil) ? @" " : firstName,(lastName == nil) ? @" " : lastName];
        [res addObject:t];
    }
    
    return res;
}

@end
