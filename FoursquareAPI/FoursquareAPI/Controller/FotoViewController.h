//
//  FotoViewController.h
//  FoursquareAPI
//
//  Created by Vovan on 05.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FotoViewController : UIViewController <UIScrollViewDelegate>

- (void) setImage:(UIImage*)image;

@end
