//
//  VenueModel.h
//  FoursquareAPI
//
//  Created by Vovan on 29.08.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VenueModelProtocol.h"

@interface VenueModel : NSObject <VenueModelProtocol>

@property(nonatomic, strong) NSMutableArray *saveVenues;

@property(nonatomic, strong) NSMutableArray *venues;

@property(nonatomic, strong) Setup *setup;

- (void) loadVenueListWithLocation:(CLLocation*) location CompletitionBlock:(LoadVenueListCompletitionBlock) block;

- (void) loadCountLikesForId:(NSString*)venueId CompletitionBlock:(LoadVenueCountLikesCompletitionBlock)block;

- (void) loadTipsForId:(NSString*)venueId CompletitionBlock:(LoadVenueTipsCompletitionBlock)block;

- (void) loadListFotosForId:(NSString*)venueId CompletitionBlock:(LoadVenueListFotosCompletitionBlock)block;

- (void) loadFotoForURL:(NSURL*)url CompletitionBlock:(LoadFotoCompletitionBlock)block;


- (NSMutableArray*) loadSavedVenue;

- (void) savedVenue:(Venue*)venue;

- (void) unsavedVenue:(Venue*)venue;


- (Setup*) loadSetup;

- (void) saveSetup:(Setup*)setup;

@end
