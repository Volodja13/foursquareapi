//
//  NameVenueTableViewCell.h
//  FoursquareAPI
//
//  Created by Vovan on 01.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Venue;

@interface NameVenueTableViewCell : UITableViewCell

- (void) setVenue:(Venue*)venue;

- (void) setCountLikes:(NSNumber*)count;

@end
