//
//  FotoTableViewCell.h
//  FoursquareAPI
//
//  Created by Vovan on 02.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FotoCollectionView;


@interface FotoTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet FotoCollectionView *fotoCollectionView;

@property (weak, nonatomic) IBOutlet UILabel *fotoLabel;

- (void) setEmptySetting;


- (void) setNormalSetting;

@end
