//
//  TipTableViewCell.h
//  FoursquareAPI
//
//  Created by Vovan on 01.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Tip;

@interface TipTableViewCell : UITableViewCell

- (void) setTip:(Tip*)tip;

+(CGFloat) heightForText:(NSString*) text;

@end
