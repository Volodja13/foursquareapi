//
//  FotoViewController.m
//  FoursquareAPI
//
//  Created by Vovan on 05.09.17.
//  Copyright © 2017 Vovan. All rights reserved.
//

#import "FotoViewController.h"

@interface FotoViewController ()
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) UIImageView *imageView;
@property (weak, nonatomic) UIImage *image;

@end

@implementation FotoViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.scrollView.delegate = self;
    
    [self.scrollView setMinimumZoomScale:1];
    [self.scrollView setMaximumZoomScale:10];
    
    self.imageView = [[UIImageView alloc] initWithFrame:self.scrollView.frame];
    [self.imageView setImage:self.image];
    [self.imageView setContentMode:UIViewContentModeScaleAspectFit];
    [self.imageView setBackgroundColor:[UIColor redColor]];
    [self.scrollView addSubview:self.imageView];
    
    int newW;
    int newH;
    float index;
    
    if (self.image.size.height>self.image.size.width) {
        index=self.scrollView.frame.size.height/self.image.size.height;
    }else{
        index=self.scrollView.frame.size.width/self.image.size.width;
    }
    
    newW=self.image.size.width*index;
    newH=self.image.size.height*index;
    
    [self.imageView setFrame:CGRectMake(0, 0, newW, newH)];
    
    [self.imageView setFrame:[self centeredFrameForScrollView:self.scrollView andUIView:self.imageView]];
    
    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapPin:)];
    doubleTap.numberOfTapsRequired = 2;
    [self.scrollView addGestureRecognizer:doubleTap];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)tapPin:(UITapGestureRecognizer *)tap
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView{
    [self.imageView setFrame:[self centeredFrameForScrollView:self.scrollView andUIView:self.imageView]];

}


- (CGRect)centeredFrameForScrollView:(UIScrollView *)scroll andUIView:(UIView *)rView
{
    CGSize boundsSize = scroll.bounds.size;
    CGRect frameToCenter = rView.frame;
    
    // горизонтально
    if (frameToCenter.size.width < boundsSize.width) {
        frameToCenter.origin.x = (boundsSize.width - frameToCenter.size.width) / 2;
    }
    else {
        frameToCenter.origin.x = 0;
    }
    // вертикально
    if (frameToCenter.size.height < boundsSize.height) {
        frameToCenter.origin.y = (boundsSize.height - frameToCenter.size.height) / 2;
    }
    else {
        frameToCenter.origin.y = 0;
    }
    return frameToCenter;
}

- (void) setImage:(UIImage*)image{
    self->_image = image;
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
    return self.imageView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
